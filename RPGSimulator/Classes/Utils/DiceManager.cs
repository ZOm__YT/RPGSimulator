﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGSimulator.Classes.Utils
{
    class DiceManager
    {

        private Random random;

        public DiceManager(Random r)
        {

            random = r;

        }

        public int ThrowDice(string diceType)
        {

            switch(diceType)
            {

                case "1D4":

                    return random.Next(1, 4+1);

                case "1D6":

                    return random.Next(1, 6+1);

                case "1D8":

                    return random.Next(1, 8+1);

                case "1D10":

                    return random.Next(1, 10 + 1);

                case "1D12":

                    return random.Next(1, 12 + 1);

                default:

                    Debug.Fail("Wrong dice type, \"" + diceType + "\" is not valid !");

                    return 0;

            }
        }

        public int[] ThrowDices(string diceType, int count)
        {

            int[] dices = new int[count];

            for(int i = 0; i < count; i++)
            {

                dices[i] = ThrowDice(diceType);

            }

            return dices;

        }

    }
}
