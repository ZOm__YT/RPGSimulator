﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGSimulator.Classes.Characters
{
    class Wizard : Character
    {
        public Wizard(string name, string diceType) : base(name, diceType)
        {
        }

        public override void Attack()
        {
            
        }

        public override void Dash()
        {
            
        }

        public override void Defend()
        {
            
        }

        public override void Heal()
        {
            
        }
    }
}
