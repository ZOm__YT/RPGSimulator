﻿using RPGSimulator.Classes.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGSimulator.Classes.Characters
{
    abstract class Character
    {

        private DiceManager dm;
        private Random cRandom;

        private string name;
        private string diceType;

        private int health, maxHealth, mana;

        private int strength, defense, agility;

        public string Name { get => name; protected set => name = value; }

        public string DiceType { get => diceType; protected set => diceType = value; }

        public int Health { get => health; set { if (value > MaxHealth) { health = MaxHealth; } else if (value < 0) { health = 0; } else { health = value; } } }
        public int MaxHealth { get => maxHealth; set => maxHealth = value; }

        public int Mana { get => mana; set => mana = value; }

        public int Strength { get => strength; protected set => strength = value; }
        public int Defense { get => defense; protected set => defense = value; }
        public int Agility { get => agility; protected set => agility = value; }

        internal DiceManager DM { get => dm; private set => dm = value; }
        public Random CRandom { get => cRandom; private set => cRandom = value; }

        public Character(string name, string diceType)
        {

            this.name = name;
            this.diceType = diceType;

            maxHealth = 10;
            health = maxHealth;

            Mana = 100;

            CRandom = new Random();

            GenerateAttributes();

        }

        private void GenerateAttributes()
        {

            int[] dices = DM.ThrowDices(DiceType, 3);

            Strength = dices[0];
            Defense = dices[1];
            Agility = dices[2];

        }

        public abstract void Attack();
        public abstract void Defend();
        public abstract void Dash();
        public abstract void Heal();

    }
}
